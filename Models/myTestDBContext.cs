﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace ReactCrudDemo.Models
{
    public partial class myTestDBContext : DbContext
    {
        public virtual DbSet<TblCities> TblCities { get; set; }
        public virtual DbSet<TblEmployee> TblEmployee { get; set; }
        // public virtual DbSet<TblEquipment> TblEquipment { get; set; }
        // public virtual DbSet<TblTypes> TblTypes { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                #warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer(@"Integrated Security=SSPI;Persist Security Info=False;Initial Catalog=Test2DB;Data Source=yusuph-HP-2000-Notebook-PC;Server=localhost; User ID=my-user-id; Password=Derickche47*;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TblCities>(entity =>
            {
                entity.HasKey(e => e.CityId);

                entity.ToTable("tblCities");

                entity.Property(e => e.CityId).HasColumnName("CityID");

                entity.Property(e => e.CityName)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            // modelBuilder.Entity<TblTypes>(entity =>
            // {
            //     entity.HasKey(e => e.TypeId);

            //     entity.ToTable("tblTypes");

            //     entity.Property(e => e.TypeId).HasColumnName("TypeID");

            //     entity.Property(e => e.TypeName)
            //         .IsRequired()
            //         .HasMaxLength(20)
            //         .IsUnicode(false);
            // });

            modelBuilder.Entity<TblEmployee>(entity =>
            {
                entity.HasKey(e => e.EmployeeId);

                entity.ToTable("tblEmployee");

                entity.Property(e => e.EmployeeId).HasColumnName("EmployeeID");

                entity.Property(e => e.City)
                    // .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Department)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Gender)
                    .IsRequired()
                    .HasMaxLength(6)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            // modelBuilder.Entity<TblEquipment>(entity =>
            // {
            //     entity.HasKey(e => e.EquipmentId);

            //     entity.ToTable("tblEquipment");

            //     entity.Property(e => e.EmployeeId).HasColumnName("EquipmentID");

            //     entity.Property(e => e.Type)
            //         .IsRequired()
            //         .HasMaxLength(20)
            //         .IsUnicode(false);

            //     entity.Property(e => e.Quantity)
            //         .IsRequired()
            //         .HasMaxLength(20)
            //         .IsUnicode(false);

            //     entity.Property(e => e.Type)
            //         .IsRequired()
            //         .HasMaxLength(6)
            //         .IsUnicode(false);

            //     entity.Property(e => e.Equipment_Name)
            //         .IsRequired()
            //         .HasMaxLength(20)
            //         .IsUnicode(false);
            // });
        }
    }
}
