﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReactCrudDemo.Models
{
    public class EmployeeDataAccessLayer
    {
        myTestDBContext db = new myTestDBContext();

        public IEnumerable<TblEmployee> GetAllEmployees()
        {
            try
            {
                return db.TblEmployee.ToList();
            }
            catch
            {
                throw;
            }
        }

        // public IEnumerable<TblEquipment> GetAllEquipment()
        // {
        //     try{
        //         return db.TblEquipment.ToList();
        //     }
        //     catch
        //     {
        //         throw;
        //     }
        // }

        //To Add new employee record   
        public int AddEmployee(TblEmployee employee)
        {
            try
            {
                db.TblEmployee.Add(employee);
                db.SaveChanges();
                return 1;
            }
            catch
            {
                throw;
            }
        }

        // public int AddEquipment(TblEquipment equipment)
        // {

        //     try
        //     {
        //         db.TblEquipment.Add(equipment);
        //         db.SaveChanges();
        //         return 1;
        //     }
        //     catch
        //     {
        //         throw;
        //     }
        // }

        //To Update the records of a particluar employee  
        public int UpdateEmployee(TblEmployee employee)
        {
            try
            {
                db.Entry(employee).State = EntityState.Modified;
                db.SaveChanges();

                return 1;
            }
            catch
            {
                throw;
            }
        }

        // public int UpdateEquipment(TblEquipment equipment)
        // {
        //     try
        //     {
        //         db.Entry(equipment).State = EntityState.Modified;
        //         db.SaveChanges();

        //         return 1;
        //     }
        //     catch
        //     {
        //         throw;
        //     }
        // }

        //Get the details of a particular employee  
        public TblEmployee GetEmployeeData(int id)
        {
            try
            {
                TblEmployee employee = db.TblEmployee.Find(id);
                return employee;
            }
            catch
            {
                throw;
            }
        }

        // public TblEquipment GetEquipmentData(int id)
        // {
        //     try
        //     {
        //         TblEquipment equipment = db.TblEquipment.Find(id);
        //         return equipment;
        //     }
        //     catch
        //     {
        //         throw;
        //     }
        // }

        //To Delete the record of a particular employee  
        public int DeleteEmployee(int id)
        {
            try
            {
                TblEmployee emp = db.TblEmployee.Find(id);
                db.TblEmployee.Remove(emp);
                db.SaveChanges();
                return 1;
            }
            catch
            {
                throw;
            }
        }

        // public int DeleteEquipment(int id)
        // {
        //     try
        //     {
        //         TblEquipment equ = db.TblEquipment.Find(id);
        //         db.TblEquipment.Remove(equ);
        //         db.SaveChanges();
        //         return 1;
        //     }
        //     catch
        //     {
        //         throw;
        //     }
        // }

        //To Get the list of Cities  
        public List<TblCities> GetCities()
        {
            List<TblCities> lstCity = new List<TblCities>();
            lstCity = (from CityList in db.TblCities select CityList).ToList();

            return lstCity;
        }

        // public List<TblTypes> GetTypes()
        // {
        //     List<TblTypes> lstType = new List<TblTypes>();
        //     lstType = (from TypeList in db.TblTypes select TypeList).ToList();

        //     return lstType;
        // }

    }
}
