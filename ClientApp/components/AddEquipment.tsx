﻿import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { Link, NavLink } from 'react-router-dom';
import { EquipmentData } from './FetchEquipment';

interface AddEquipmentDataState {
    title: string;
    loading: boolean;
    typeList: Array<any>;
    equData: EquipmentData;
}

export class AddEquipment extends React.Component<RouteComponentProps<{}>, AddEquipmentDataState> {
    constructor(props) {
        super(props);

        this.state = { title: "", loading: true, typeList: [], equData: new EquipmentData };

        fetch('api/Equipment/GetTypeList')
            .then(response => response.json() as Promise<Array<any>>)
            .then(data => {
                this.setState({ typeList: data });
            });

        var empid = this.props.match.params["empid"];

        // This will set state for Edit employee
        if (empid > 0) {
            fetch('api/Equipment/Details/' + empid)
                .then(response => response.json() as Promise<EquipmentData>)
                .then(data => {
                    this.setState({ title: "Edit", loading: false, equData: data });
                });
        }

        // This will set state for Add employee
        else {
            this.state = { title: "Create", loading: false, typeList: [], equData: new EquipmentData };
        }

        // This binding is necessary to make "this" work in the callback
        this.handleSave = this.handleSave.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
    }

    public render() {
        let contents = this.state.loading
            ? <p><em>Loading fetching data...</em></p>
            : this.renderCreateForm(this.state.typeList);

        return <div>
            <h1>{this.state.title}</h1>
            <h3>Equipments</h3>
            <hr />
            {contents}
        </div>;
    }

    // This will handle the submit form event.
    private handleSave(event) {
        event.preventDefault();
        const data = new FormData(event.target);

        // PUT request for Edit employee.
        if (this.state.equData.equipmentId) {
            fetch('api/Equipment/Edit', {
                method: 'PUT',
                body: data,

            }).then((response) => response.json())
                .then((responseJson) => {
                    this.props.history.push("/fetchequipment");
                })
        }

        // POST request for Add employee.
        else {
            fetch('api/Equipment/Create', {
                method: 'POST',
                body: data,

            }).then((response) => response.json())
                .then((responseJson) => {
                    this.props.history.push("/fetchequipment");
                })
        }
    }

    // This will handle Cancel button click event.
    private handleCancel(e) {
        e.preventDefault();
        this.props.history.push("/fetchequipment");
    }

    // Returns the HTML Form to the render() method.
    private renderCreateForm(typeList: Array<any>) {
        return (
            <form onSubmit={this.handleSave} >
                <div className="form-group row" >
                    <input type="hidden" name="equipmentId" value={this.state.equData.equipmentId} />
                </div>
                < div className="form-group row" >
                    <label className=" control-label col-md-12" htmlFor="Name">Name</label>
                    <div className="col-md-4">
                        <input className="form-control" type="text" name="name" defaultValue={this.state.equData.equipment_name} required />
                    </div>
                </div >
                <div className="form-group row">
                    <label className="control-label col-md-12" htmlFor="Gender">Gender</label>
                    <div className="col-md-4">
                        <select className="form-control" data-val="true" name="gender" defaultValue={this.state.equData.type} required>
                            <option value="">-- Select Type --</option>
                            <option value="Male">Hammer</option>
                            <option value="Female">Shovel</option>
                        </select>
                    </div>
                </div >
                <div className="form-group row">
                    <label className="control-label col-md-12" htmlFor="Department" >Quantity</label>
                    <div className="col-md-4">
                        <input className="form-control" type="text" name="Department" defaultValue={this.state.equData.quantity} required />
                    </div>
                </div>
                <div className="form-group row">
                    <label className="control-label col-md-12" htmlFor="City">City</label>
                    <div className="col-md-4">
                        <select className="form-control" data-val="true" name="City" defaultValue={this.state.equData.type} required>
                            <option value="">-- Select City --</option>
                            {typeList.map(city =>
                                <option key={city.cityId} value={city.cityName}>{city.cityName}</option>
                            )}
                        </select>
                    </div>
                </div >
                <div className="form-group">
                    <button type="submit" className="btn btn-default">Save</button>
                    <button className="btn" onClick={this.handleCancel}>Cancel</button>
                </div >
            </form >
        )
    }
}