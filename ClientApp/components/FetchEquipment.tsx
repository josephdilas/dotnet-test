﻿import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { Link, NavLink } from 'react-router-dom';

interface FetchEquipmentDataState {
    equList: EquipmentData[];
    loading: boolean;
}

export class FetchEquipment extends React.Component<RouteComponentProps<{}>, FetchEquipmentDataState> {
    constructor() {
        super();
        this.state = { equList: [], loading: true };

        fetch('api/Equipment/Index')
            .then(response => response.json() as Promise<EquipmentData[]>)
            .then(data => {
                this.setState({ equList: data, loading: false });
            });

       // This binding is necessary to make "this" work in the callback
        this.handleDelete = this.handleDelete.bind(this);
        this.handleEdit = this.handleEdit.bind(this);

    }

    public render() {
        let contents = this.state.loading
            ? <p><em>Loading collecting...</em></p>
            : this.renderEquipmentTable(this.state.equList);

        return <div>
            <h1>Equipments Data</h1>
            <p>This component demonstrates fetching Equipment data from the server.</p>
            <p>
                <Link to="/addequipment">Create New</Link>
            </p>
            {contents}
        </div>;
    }

    // Handle Delete request for an employee
    private handleDelete(id: number) {
        if (!confirm("Do you want to delete equipment with Id: " + id))
            return;
        else {
            fetch('api/Equipment/Delete/' + id, {
                method: 'delete'
            }).then(data => {
                this.setState(
                    {
                        equList: this.state.equList.filter((rec) => {
                            return (rec.equipmentId != id);
                        })
                    });
            });
        }
    }

    private handleEdit(id: number) {
        this.props.history.push("/equipment/edit/" + id);
    }

    // Returns the HTML table to the render() method.
    private renderEquipmentTable(equList: EquipmentData[]) {
        return <table className='table'>
            <thead>
                <tr>
                    <th></th>
                    <th>EquipmentsID</th>
                    <th>Equipment Name</th>
                    <th>Type</th>
                    <th>Quantity</th>
                    {/* <th>City</th> */}
                </tr>
            </thead>
            <tbody>
                {equList.map(emp =>
                    <tr key={emp.equipmentId}>
                        <td></td>
                        <td>{emp.equipmentId}</td>
                        <td>{emp.equipment_name}</td>
                        <td>{emp.type}</td>
                        <td>{emp.quantity}</td>
                        {/* <td>{emp.city}</td> */}
                        <td>
                            <a className="action" onClick={(id) => this.handleEdit(emp.equipmentId)}>Edit</a>  |
                            <a className="action" onClick={(id) => this.handleDelete(emp.equipmentId)}>Delete</a>
                        </td>
                    </tr>
                )}
            </tbody>
        </table>;
    }
}

export class EquipmentData {
    equipmentId: number = 0;
    equipment_name: string = "";
    type: string = "";
    // city: string = "";
    quantity: string = "";
} 